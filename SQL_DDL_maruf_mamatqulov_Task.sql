
-- NOTE I used SQL-shell (psql). Because it is more comfortable for me!


-- Create the database
CREATE DATABASE itpu_students;
-- connecting datadabe
\c itpu_students;

-- Create a schema
CREATE SCHEMA itpu_schema;

-- Set search_path to the newly created schema
SET search_path TO itpu_schema;

CREATE TABLE students (
    student_id SERIAL NOT NULL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    gender CHAR(10) CHECK (gender IN ('Male', 'Female')),
    date_of_birth DATE CHECK (date_of_birth > '2000-01-01'),
    email VARCHAR(100) UNIQUE, -- Add the email column
    CONSTRAINT unique_student_email UNIQUE (email)
);


CREATE TABLE courses (
    course_id SERIAL PRIMARY KEY,
    course_name VARCHAR(100) NOT NULL,
    credits INT NOT NULL CHECK (credits > 0),
    course_code VARCHAR(20) UNIQUE, -- Add the course_code column
    CONSTRAINT unique_course_code UNIQUE (course_code)
);



CREATE TABLE enrollments (
    enrollment_id SERIAL PRIMARY KEY,
    student_id INT REFERENCES students(student_id),
    course_id INT REFERENCES courses(course_id),
    enrollment_date DATE DEFAULT CURRENT_DATE,
    CONSTRAINT unique_enrollment UNIQUE (student_id, course_id)
);

-- Add record_ts field to each table
ALTER TABLE students ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE courses ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;
ALTER TABLE enrollments ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_DATE;

-- Check if record_ts has been set for existing rows
UPDATE students SET record_ts = DEFAULT WHERE record_ts IS NULL;
UPDATE courses SET record_ts = DEFAULT WHERE record_ts IS NULL;
UPDATE enrollments SET record_ts = DEFAULT WHERE record_ts IS NULL;



-- Tables with sample data

INSERT INTO students (first_name, last_name, gender, date_of_birth, email) 
VALUES ('Maruf', 'Mamatqulov', 'Male', '2004-06-01', 'maruf_mamatqulov@student.itpu.uz'),
       ('Alisher', 'Rapiyev', 'Male', '2001-02-28', 'alisher_rapiyev@student.itpu.uz'),
       ('Tuxtasin', 'Ikromov', 'Male', '2002-04-18', 'tuxtasin_ikromov@student.itpu.uz'),
       ('Fotima', 'Ergashova', 'Female', '2004-03-28', 'fotima_ergashova@student.itpu.uz'),
       ('Ziyodulla', 'Bayxonov', 'Male', '2002-08-11', 'ziyodulla_bayxonov@student.itpu.uz');

-- Tables with sample data

INSERT INTO courses (course_name, credits, course_code) 
VALUES 
    ('Data Structures', 5, 'DS201'),
    ('Mathematics', 5, 'SE301'),
    ('Computer Networks', 3, 'CN101'),
    ('Database', 5, 'DBM101'),
    ('Web Development', 4, 'WEB202'),
    ('Operating Systems', 5, 'OS301'),
    ('OOP', 3, 'OP402');

-- Tables with sample data


INSERT INTO enrollments (student_id, course_id) 
VALUES 
    (1, 3),
    (2, 4),
    (1, 5),
    (2, 3),
    (3, 1),
    (4, 2), 
    (5, 1),
    (3, 4); 





-- checking examles

SELECT * FROM students;  -- or SELECT * FROM tpu_schema.students s;
SELECT * FROM enrollments; -- or SELECT * FROM tpu_schema.enrollments e;
SELECT * FROM courses; -- or SELECT * FROM tpu_schema.courses c;

-- and with INNER JOIN 

SELECT
    s.first_name,
    s.last_name,
    c.course_name
FROM
    students s
JOIN enrollments e ON s.student_id = e.student_id
JOIN courses c ON e.course_id = c.course_id;



